using NUnit.Framework;

namespace csharp_blank_project
{
    class ProgramTest {

        [TestCase]
        public void should_return_true()
        {
            bool actual = new Program().Generate();
            Assert.AreEqual(true, actual);
        }
    }
}